# config valid only for current version of Capistrano
lock "3.8.1"

set :application, "edisco"
set :repo_url, "git@gitlab.com:edisco/cloud-admin.git"

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp
# set :branch, :release
# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, "~/projects/edisco-admin"

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: "log/capistrano.log", color: :auto, truncate: :auto

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
append :linked_files, ".env", "puma.rb"

# Default value for linked_dirs is []
append :linked_dirs, "log", "tmp/pids", "tmp/cache","tmp/puma", "tmp/sockets", "public/system", "certs"

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5

# set :puma_jungle_conf, '/etc/puma.conf'
# set :puma_run_path, '/usr/local/bin/run-puma'
set :puma_state, "#{shared_path}/tmp/puma/state"
set :puma_pid, "#{shared_path}/tmp/puma/pid"
set :puma_preload_app, true
# set :puma_conf, "#{Rails.root}/config/puma.conf"
# append :puma_conf, "#{current_path}/config/puma.rb"
set :rails_env, 'production'