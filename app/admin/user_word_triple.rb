ActiveAdmin.register UserWordTriple do
  permit_params :text, :part, :gender
  controller do
    def scoped_collection
      UserWordTriple.includes(:user)
      UserWordTriple.includes(:front1)
      UserWordTriple.includes(:front2)
      UserWordTriple.includes(:front3)
      UserWordTriple.includes(:back)
    end
  end  

  index do
    selectable_column
    id_column
    column :uuid
    column :user do |r|
    	r.user && r.user.email
    end
    column :front1 do |r|
    	r.front1 && r.front1.word.text
    end
    column :front2 do |r|
    	r.front2 && r.front2.word.text
    end
    column :front1 do |r|
    	r.front3 && r.front3.word.text
    end
    column :back do |r|
    	r.back && r.back.word.text
    end
    
    column :version
    column :trashed
    column :created_at

    actions
  end
  filter :user_email_cont, :as => :string
  
  filter :front1_word_text_cont, :as => :string
  filter :front2_word_text_cont, :as => :string
  filter :front3_word_text_cont, :as => :string
  filter :back_word_text_cont, :as => :string

  filter :created_at

  form do |f|
    f.inputs "Admin Details" do
      f.input :text
      f.input :part
      f.input :gender
    end
    f.actions
  end

end
