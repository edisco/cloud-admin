ActiveAdmin.register Log do
  controller do
    def scoped_collection
      Log.includes(:user)
    end
  end

  index do
    selectable_column
    id_column
    column :user do |r|
      r.user && r.user.email
    end

    column :created_at
    column :title
    column :msg
    column 'backtrace' do |log|
      truncate log.backtrace, length: 50, omission: '...'
    end
    column :level
    actions
  end
  filter :user_email_cont, :as => :string
  filter :title
  filter :level
  
  filter :created_at


end
