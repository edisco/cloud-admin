ActiveAdmin.register UserWord do
  permit_params :text, :part, :gender
  controller do
    def scoped_collection
      UserWord.includes(:user)
      UserWord.includes(:word)
    end
  end  
  index do
    selectable_column
    id_column
    column :uuid
    column :user do |r|
    	r.user && r.user.email
    end
    column :text do |r|
    	r.word && r.word.text
    end
    column :part do |r|
    	r.word && r.word.part
    end
    column :gender do |r|
    	r.word && r.word.gender
    end
    column :language do |r|
    	r.word && r.word.language && r.language.lang || 'NONE!'
    end
    column :version
    column :trashed
    column :created_at

    actions
  end
  filter :uuid_eq,:as => :string
  filter :user_email_cont, :as => :string
  filter :word_text_cont, :as => :string
  filter :uuid
  filter :text
  filter :part
  filter :gender
  filter :created_at

  form do |f|
    f.inputs "Admin Details" do
      f.input :text
      f.input :part
      f.input :gender
    end
    f.actions
  end

end
