ActiveAdmin.register Word do
  permit_params :text, :part, :gender

  index do
    selectable_column
    id_column
    column :uuid
    column :text
    column :created_at
    column :language do |r|
    	r.language && r.language.lang || 'NONE!'
    end
    column :part
    column :gender
    column :created_at
    actions
  end
	filter :uuid_eq,:as => :string
  filter :text
  filter :part
  filter :gender
  filter :created_at

  form do |f|
    f.inputs "Admin Details" do
      f.input :text
      f.input :part
      f.input :gender
    end
    f.actions
  end

end
