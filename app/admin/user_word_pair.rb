ActiveAdmin.register UserWordPair do
  permit_params :text, :part, :gender
  controller do
    def scoped_collection
      UserWordPair.includes(:user)
      UserWordPair.includes(:front)
      UserWordPair.includes(:back)
    end
  end  

  index do
    selectable_column
    id_column
    column :uuid
    column :user do |r|
    	r.user && r.user.email
    end
    column :front do |r|
    	r.front && r.front.word.text
    end
    column :back do |r|
    	r.back && r.back.word.text
    end
    
    column :version
    column :trashed
    column :created_at

    actions
  end
  filter :user_email_cont, :as => :string
  
  filter :front_word_text_cont, :as => :string
  filter :back_word_text_cont, :as => :string

  filter :created_at

  form do |f|
    f.inputs "Admin Details" do
      f.input :text
      f.input :part
      f.input :gender
    end
    f.actions
  end

end
