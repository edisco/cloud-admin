# frozen_string_literal: true

class Log < ApplicationRecord
  belongs_to :user
  validates :user, presence: true

  def self.warn(**params)
    log params.merge(level: 10)
    Rails.logger.warn params
  end

  def self.error(**params)
    log params.merge(level: 20)
    Rails.logger.error params
  end

  def self.safe_error(**params)
    error params
  rescue Exception => e
    begin
       Rails.logger.error "Log.safe_error: #{e.message}, params: #{params}"
     rescue
     end
  end

  private

  def self.log(**params)
    Log.create! params
  end
end
