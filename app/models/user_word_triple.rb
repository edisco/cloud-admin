# frozen_string_literal: true

class UserWordTriple < ApplicationRecord
  PERMIT_PARAMS = [
    :uuid,
    data: [
      :type,
      :uuid,
      attributes: %i[front1_uuid front2_uuid front3_uuid back_uuid version trashed stage created_at]
    ]
  ].freeze
  belongs_to :word_triple
  belongs_to :user
  belongs_to :front1, class_name: 'UserWord'
  belongs_to :front2, class_name: 'UserWord'
  belongs_to :front3, class_name: 'UserWord'
  belongs_to :back, class_name: 'UserWord'

  has_many :user_word_triple_histories, dependent: :destroy

  def to_api
    {
      type: self.class.name,
      uuid: uuid,
      attributes: {
        front1_uuid: front1.uuid,
        front2_uuid: front2.uuid,
        front3_uuid: front3.uuid,
        back_uuid: back.uuid,
        trashed: trashed,
        version: version,
        created_at: created_at.to_s(:iso8601ms),
        stage: stage
      }
    }
  end
end
