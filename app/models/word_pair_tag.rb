# frozen_string_literal: true

class WordPairTag < ApplicationRecord
  belongs_to :tag
  belongs_to :word_pair
end
