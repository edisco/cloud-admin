# frozen_string_literal: true

class UserWordPair < ApplicationRecord
  PERMIT_PARAMS = [
    :uuid,
    data: [
      :type,
      :uuid,
      attributes: %i[front_uuid back_uuid version trashed order stage created_at]
    ]
  ].freeze
  belongs_to :user
  belongs_to :word_pair
  belongs_to :front, class_name: 'UserWord'
  belongs_to :back, class_name: 'UserWord'
  has_many :user_word_pair_histories, dependent: :destroy

  def to_api
    {
      type: self.class.name,
      uuid: uuid,
      attributes: {
        front_uuid: front.uuid,
        back_uuid: back.uuid,
        trashed: trashed,
        version: version,
        order: order,
        stage: stage,
        created_at: created_at.to_s(:iso8601ms)
      }
    }
  end
end
