# frozen_string_literal: true

class WordPair < ApplicationRecord
  has_many :word_pair_tags
  has_many :tags, through: :word_pair_tags
  belongs_to :front, class_name: 'Word'
  belongs_to :back, class_name: 'Word'
end
