# frozen_string_literal: true

class Word < ApplicationRecord
  belongs_to :language
  has_many :word_pairs, foreign_key: :front_id
  def lang=(value)
    self.language = Language.find_or_create_by(lang: value)
  end
end
