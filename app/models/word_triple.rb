# frozen_string_literal: true

class WordTriple < ApplicationRecord
  belongs_to :front1, class_name: 'Word'
  belongs_to :front2, class_name: 'Word'
  belongs_to :front3, class_name: 'Word'
  belongs_to :back, class_name: 'Word'
end
