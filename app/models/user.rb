# frozen_string_literal: true

class User < ApplicationRecord

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, #:registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :word_triple_histories, class_name: 'UserWordTripleHistory', dependent: :destroy
  has_many :word_pair_histories, class_name: 'UserWordPairHistory', dependent: :destroy
  has_many :word_pairs, class_name: 'UserWordPair', dependent: :destroy
  has_many :word_triples, class_name: 'UserWordTriple', dependent: :destroy
  has_many :words, class_name: 'UserWord', dependent: :destroy
  has_many :logs, dependent: :destroy


end
