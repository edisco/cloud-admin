# frozen_string_literal: true

class UserWord < ApplicationRecord
  PERMIT_PARAMS = [
    data:	[
      :type,
      :uuid,
      attributes: %i[text lang version gender part trashed created_at]
    ]
  ].freeze

  belongs_to :word
  belongs_to :user
  has_one :language, through: :word
  has_many :user_word_pairs_as_front, foreign_key: :front_id, class_name: 'UserWordPair', dependent: :destroy
  has_many :user_word_pairs_as_back, foreign_key: :back_id, class_name: 'UserWordPair', dependent: :destroy

  has_many :user_word_triples_as_front1, foreign_key: :front1_id, class_name: 'UserWordTriple', dependent: :destroy
  has_many :user_word_triples_as_front2, foreign_key: :front2_id, class_name: 'UserWordTriple', dependent: :destroy
  has_many :user_word_triples_as_front3, foreign_key: :front3_id, class_name: 'UserWordTriple', dependent: :destroy
  has_many :user_word_triples_as_back, foreign_key: :back_id, class_name: 'UserWordTriple', dependent: :destroy

  def to_api
    {
      type: self.class.name,
      uuid: uuid,
      attributes: {
        text: word.text,
        lang: word.language.lang,
        version: version,
        gender: word.gender,
        part: word.part,
        trashed: trashed,
        created_at: created_at.to_s(:iso8601ms)
      }
    }
  end
end
