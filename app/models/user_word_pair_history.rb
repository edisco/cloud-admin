# frozen_string_literal: true

class UserWordPairHistory < ApplicationRecord
  PERMIT_PARAMS = [
    :uuid,
    data: [
      :type,
      :uuid,
      attributes: %i[
        correct
        word_pair_uuid
        trashed
        version
        created_at
      ]
    ]
  ].freeze
  belongs_to :user_word_pair
  belongs_to :user

  def to_api
    {
      type: self.class.name,
      uuid: uuid,
      attributes: {
        word_pair_uuid: user_word_pair.uuid,
        correct: correct,
        created_at: created_at.to_s(:iso8601ms),
        trashed: trashed,
        version: version
      }
    }
  end
end
