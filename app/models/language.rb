# frozen_string_literal: true

class Language < ApplicationRecord
  ENG = 'en'
  RUS = 'ru'
  GER = 'de'

  def self.eng
    Language.find_or_create_by lang: ENG
  end

  def self.rus
    Language.find_or_create_by lang: RUS
  end

  def self.ger
    Language.find_or_create_by lang: GER
  end

  def self.[](value)
    Language.constants(false).each do |const|
      return Language.find_or_create_by lang: value if Language.const_get(const) == value
    end
    raise "Unable to find language for #{value}"
  end
end
